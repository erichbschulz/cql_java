
The purpose of this repo is to support deploying the java tools for the CQL project

   https://github.com/cqframework/clinical_quality_language/blob/master/Src/java/README.md

After intstalling docker, the build and run commands are a bit like this:

    CQF_CQL=<<path to the CQL project repo>>
    docker build -t cqljava .
    docker run -it -v $CQF_CQL:/usr/src/myapp \
      -w /usr/src/myapp --name javao cqljava

The docker `build` operation is relatively download intensive but one-off. The
`run` operation mounts the project repo as a volume to the working directory
and opens a shell. Docker will reflect changes made in this mounted volume
accros the docker container and the host.

Once a docker has been started once you can restart it with:

    docker restart javao

And then `ssh` into the container with:

    docker exec -it javao bash

Once you have opened a shell on the docker you can build the java file with:

    cd Src/java
    ./gradlew build
    ./gradlew :cql-to-elm:installApp

The command to convert files is then simply (as documented [here](https://github.com/cqframework/clinical_quality_language/blob/master/Src/java/README.md#generate-an-elm-representation-of-cql-logic):

    cd Src/java
    ./cql-to-elm/build/install/cql-to-elm/bin/cql-to-elm --input ../../Examples/ChlamydiaScreening_CQM.cql --output temp.json --f=JSON

or

    Src/java/cql-to-elm/build/install/cql-to-elm/bin/cql-to-elm --input Examples/library-cbp.cql

